import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html'
})
export class HeroeTarjetaComponent implements OnInit {

    @Input() heroe:any[] = [];
    @Input() indice:number;

    @Output() heroeSlected: EventEmitter<number>;

    constructor(private ruta:Router) {
        this.heroeSlected = new EventEmitter();
    }

  ngOnInit() {
  }

    verHeroe() {
        // console.log(this.indice);
        this.ruta.navigate( ['/heroe',this.indice] );
        // this.heroeSlected.emit( this.indice );
    }
}
