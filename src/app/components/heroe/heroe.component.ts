import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesServiceService } from 'src/app/servicios/heroes.service.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: []
})
export class HeroeComponent implements OnInit {

    heroe:any = {};

    constructor( private actuvatedRouet:ActivatedRoute, private _heroeServie:HeroesServiceService ) {
        this.actuvatedRouet.params.subscribe(data=>{
            console.log( data );
            this.heroe = this._heroeServie.getHeroe( data['id'] );
            console.log( this.heroe );
        })
    }

  ngOnInit() {
  }

}
