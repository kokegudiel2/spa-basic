import { Component, OnInit } from '@angular/core';
import { HeroesServiceService } from 'src/app/servicios/heroes.service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

    heroes:any[] = [];

    constructor( private _heroesService:HeroesServiceService, private ruta:Router ) { }

    ngOnInit() {
        this.heroes = this._heroesService.getHeroes();

        console.log("Jopende!!", this.heroes);
    }

    verHeroe(id:number) {
        this.ruta.navigate( ['/heroe',id] );
    }
}
