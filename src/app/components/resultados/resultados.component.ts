import { Component, OnInit } from '@angular/core';
import { HeroesServiceService } from 'src/app/servicios/heroes.service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styles: []
})
export class ResultadosComponent implements OnInit {

    heroes:any[] = [];

    constructor( private servicio:HeroesServiceService, private activatedRoute:ActivatedRoute ) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(data => {
            this.heroes = this.servicio.findHeroes( data['termino'] );
        })
    }

}
